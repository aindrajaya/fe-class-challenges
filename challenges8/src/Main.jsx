import React from 'react'
import AuthPage from "./pages/Auth"
import EditNotePage from "./pages/EditNote"
import NotesPage from "./pages/Notes"
import {Switch, Route, Redirect} from 'react-router-dom'

const Main = () => {
    return(
        <div className="container my-5">
            <Switch>
                <Route exact path="/auth" component={AuthPage} />
                <Route exact path="/notes" component={NotesPage}/>
                <Route exact path="/edit-note" component={EditNotePage}/>
                <Route exact path="/edit-note/:noteId" component={EditNotePage} />
                <Redirect to="/auth"/>
            </Switch>
        </div>
    )
}

export default Main