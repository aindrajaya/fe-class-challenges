import React from "react"
import {Switch, Route, Redirect} from 'react-router-dom'
import {connect} from 'react-redux'
import 'react-toastify/dist/ReactToastify.css'
import {ToastContainer, Slide} from 'react-toastify'
import AuthPage from "./pages/Auth"
import EditNotePage from "./pages/EditNote"
import NotesPage from "./pages/Notes"
import HeaderComponent from "./components/Header"
import Spinner from "./components/spinner/Spinner"
import {logoutUser} from './redux/actions/authActionCreators'

const App = ({user, dispatchLogoutAction}) => {
    return(
        <React.Fragment>
            <ToastContainer position="top-right" autoClose={2000} hideProgressBar transition={Slide}/>
            <Spinner />
            <HeaderComponent isLoggedIn={user.isLoggedIn} userName={user.fullName} onLogout={dispatchLogoutAction}/>
            <div className="container my-5">
                {user.isLoggedIn ? 
                    (
                        <Switch>
                            <Route exact path="/notes" component={NotesPage}/>
                            <Route exact path="/edit-note" component={EditNotePage}/>
                            <Route exact path="/edit-note/:noteId" component={EditNotePage} />
                            <Redirect to="/notes"/>
                        </Switch> 
                    ) : (
                        <Switch Switch>
                            <Route exact path="/auth" component={AuthPage} />
                            <Redirect to="/auth"/>
                        </Switch>
                    )
                }
            </div>
        </React.Fragment>
    )
}

const mapStateToProps = (state) => ({user: state.user})
const mapDispatchToProps = (dispacth) =>({
    dispatchLogoutAction : () => dispacth(logoutUser())
})

export default connect(mapStateToProps, mapDispatchToProps)(App)
   