import React, {useEffect} from 'react'
import {connect} from 'react-redux'

import NotesCollectionComponent from '../components/NotesCollection'
import {fetchAllNotes} from './../redux/actions/notesActionCreators'


const NotesPage = ({loading, notes, dispatchFetchAllNotesAction}) => {
    
    useEffect(() => dispatchFetchAllNotesAction(), [dispatchFetchAllNotesAction])
    
    return(
        <React.Fragment>
            <NotesCollectionComponent notes={notes} />
        </React.Fragment>
    )
}

const mapStateToProps = state => ({
    loading: state.loading,
    notes: state.notes
})

const mapDispatchToProps = dispatch => ({
    dispatchFetchAllNotesAction = () => dispatch(fetchAllNotes())
})

export default connect(mapDispatchToProps, mapStateToProps)(NotesPage)