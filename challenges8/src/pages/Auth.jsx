import React from 'react'
import LoginFormComponent from '../components/Login'
import RegisterFormComponent from '../components/Register'



const AuthPage = () => {
    return(
        <div className="row justify-content-between">
            <div className="col-md-5">
                <LoginFormComponent />
            </div>

            <div style={{border:'1px solid #ababab'}}></div>

            <div className="col-md-6">
                <RegisterFormComponent />
            </div>
        </div>
    )
}

export default AuthPage