import React from 'react'



const NotesCollectionComponent = ({notes}) => {
    return(
        <React.Fragment>
            <table className = "table table-hover">
                <thead className="thead-dark">
                    <tr>
                        <th scope="col">Judul</th>
                        <th scope="col">Isi</th>
                        <th scope="col">Diskripsi</th>
                        <th scope="col">Kategori</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        notes.map(item => (
                            <tr key={item._id}>
                                <td>{item.title}</td>
                                <td>{item.content}</td>
                                <td>{item.description}</td>
                                <td>{item.category}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </React.Fragment>
    )
}

export default NotesCollectionComponent