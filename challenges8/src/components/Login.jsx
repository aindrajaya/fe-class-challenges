import React, {useState} from 'react'
import {connect} from 'react-redux'
import {toast} from 'react-toastify'

import {loginUser} from '../redux/actions/authActionCreators'

const LoginFormComponent = ({dispatchLoginAction}) => {
    const[email, setEmail] = useState('')
    const[password, setPassword] = useState('')

    const handleOnSubmit = (event) => {
        event.preventDefault()
        dispatchLoginAction(email, password, 
            () => toast.success('Berhasil Masuk'), 
            (message) => toast.error(`Error: ${message}`))
    }

    return(
        <React.Fragment>
            <h2>Sudah Mendaftar ?</h2>
            <h4>Masuk disini</h4>
            <br/>

            <form noValidate onSubmit={handleOnSubmit}>
                <div className="form-group">
                    <label htmlFor="email">Email Address</label>
                    <input noValidate 
                        type="email"
                        name="email"
                        placeholder="Masukan Email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        className="form-control"
                        />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Password</label>
                    <input noValidate 
                        type="password"
                        name="password"
                        placeholder="Masukan Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        className="form-control"
                        />
                </div>
                
                <button type="submit" className="btn btn-primary mr-2">
                    Masuk | <i className="fas fa-sign-in-alt"></i>
                </button>
                <button className="btn btn-outline-secondary">
                    Cancel | <i className="fas fa-times"></i>
                </button>
            </form>

        </React.Fragment>
    )
}

const mapDispatchToProps = dispatch => ({
    dispatchLoginAction: (email, password, onSuccess, onError) =>
        dispatch(loginUser({ email, password }, onSuccess, onError))
});

export default connect(null, mapDispatchToProps)(LoginFormComponent)