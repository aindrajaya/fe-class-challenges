import React, {useState} from 'react'
import {connect} from 'react-redux'
import {toast} from 'react-toastify'

import { registerUser } from '../redux/actions/authActionCreators'

const RegisterFormComponent = ({dispatchRegisterAction}) => {

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const handleOnSubmit = (event) => {
        event.preventDefault()
        dispatchRegisterAction(firstName, lastName, email, password, 
            () => toast.success('Akun berhasil dibuat'),
            (message) => toast.error(`Error: ${message}`))
    }
    
    return(
        <React.Fragment>
            <h2>Pengguna Baru?</h2>
            <h4>Daftar disini</h4>
            <br/>

            <form noValidate onSubmit={handleOnSubmit}>
                <div className="form-group">
                    <label htmlFor="firstName">Nama Depan</label>
                    <input noValidate
                        id="firstName"
                        type="text"
                        name="firstName"
                        placeholder="Nama Depan"
                        value= {firstName}
                        onChange = {(e) => setFirstName(e.target.value)}
                        className="form-control"
                        />
                </div>
                <div className="form-group">
                    <label htmlFor="lastName">Nama Belakang</label>
                    <input noValidate
                        id="lastName"
                        type="text"
                        name="lastName"
                        placeholder="Nama Belakang"
                        value= {lastName}
                        onChange = {(e) => setLastName(e.target.value)}
                        className="form-control"
                        />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input noValidate
                        id="email"
                        type="text"
                        name="email"
                        placeholder="Alamat Email"
                        value= {email}
                        onChange = {(e) => setEmail(e.target.value)}
                        className="form-control"
                        />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input noValidate
                        id="password"
                        type="password"
                        name="password"
                        placeholder="Kata Sandi"
                        value= {password}
                        onChange = {(e) => setPassword(e.target.value)}
                        className="form-control"
                        />
                </div>

                <button type="submit" className="btn btn-primary mr-2">
                    Daftar | <i className="fas fa-user-plus"></i>
                </button>
                <button className="btn btn-outline-secondary">
                    Cancel | <i className="fas fa-times"></i>
                </button>
            </form>
        </React.Fragment>
    )
}

const mapDispatchToProps = dispatch => ({
    dispatchRegisterAction : (firstName, lastName, email, password, onSuccess, onError) =>
        dispatch(registerUser({firstName, lastName, email, password}, onSuccess, onError))
})

export default connect(null, mapDispatchToProps)(RegisterFormComponent)