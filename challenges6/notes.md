1. Given a stateless functional component, add state to it
2. state should have a property called `isLoggedIn` which is a boolean
3. (true if logged in, false if not)
4. Then, give your best shot at rendering the word "in" if the user is logged in
5. or "out" if the user is logged out.
6. Add some button below the h1
7. Make it clickable (return "in/out" in the h1 element) by using setState and handleClick methods