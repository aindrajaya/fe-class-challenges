import reducer from '../reducers';
import { createStore } from 'redux';

const initialState = {tech: ""};

export const store = createStore(reducer, initialState);
