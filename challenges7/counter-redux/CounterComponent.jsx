import React from "react"
// don't forget to use Connect from react-redux

class CounterComponent extends React.Component{
    //remove this state too
    state = {
        count: 0
    }

    increment = () => {
        this.setState(state => ({
            count: state.count + 1
        }))
      //remove this code above and  code to consume dispatch actions type Increment should be here
    }

    decrement = () => {
        this.setState(state => ({
            count: state.count - 1
        }))
      //remove this code above and code to consume dispatch actions type Decrement should be here
    }

    render(){
        return(
            <div className="Counter">
                <h2>Counter</h2>
                <div>
                    <button onClick={this.decrement}>-</button>
                    {/* change to props */}
                    <span className="counter"> {this.state.count} </span>
                    <button onClick={this.increment}>+</button>
                </div>
            </div>
        )
    }

    //added mapStateToProps function here
}

export default CounterComponent