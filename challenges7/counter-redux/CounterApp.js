import React from "react"
import CounterComponent from "./CounterComponent"
//don't forget to add createStore from redux

//set the initial state

//set the reducer
//on the reducer don't forget to add actions with switch statements

//set the createStore

class CounterApp extends React.Component {
    // dont't forget change to provider to consume the Counter component
    render(){
        return(
            <div>
            <CounterComponent />
        </div>
        )
    }
}

export default CounterApp