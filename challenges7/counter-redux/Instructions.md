1. Add redux(store and update) and react-redux(connect redux to react)
2. Remove state and update functions
3. change the state in the render jsx to props because it will be consume from redux
4. Go index to check the props work properly by passing the component value
5. import createStore and declare store = createStore() [it will cause error reducer]
6. create the function reducer
7. declare initialState count: 0
8. prove that createStore will call reducer first time
9. on the reducer please write initialState
10. and then print out the value of state by write getState() function
11. the reducer basically receive 2 arguments, state and action
12. next you will see the type is not structured well
13. and then you can dispatch from store to a specifcic type (INCREMENT)
14. After you dispatch from store you can fill the action from the reducer with the simple switch case which is return new state that will be added + for the count and the - will be decrease the state value
15. And then you must be integrating the counter component from the redux store by using connect function from react-redux
16. After that you must be call the state from the index main counter index to the props of the + - component by using mapStateToProps function and return value count: state.count	
17. the next you must intergrate the increment function to consume the dispatch type with an INCREMENT and DECREMENT actions
18. So, That's all